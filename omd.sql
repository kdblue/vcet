-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 01, 2016 at 09:33 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `password`) VALUES
('nikhil', 'nikhil25'),
('kuldeep3', 'kdblue3'),
('mihir', 'mihir25'),
('nikhil', 'nikhil25'),
('kuldeep3', 'kdblue3'),
('mihir', 'mihir25'),
('kdblue', 'kdblue3');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_info`
--

CREATE TABLE IF NOT EXISTS `medicine_info` (
  `medicine_id` int(5) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(30) NOT NULL,
  `medicine_price` int(4) NOT NULL,
  PRIMARY KEY (`medicine_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `medicine_info`
--

INSERT INTO `medicine_info` (`medicine_id`, `company_name`, `medicine_price`) VALUES
(1, 'Ranbaxy', 10),
(2, 'Cipla', 20),
(3, 'Himalaya', 30),
(4, 'Cipla', 40),
(5, 'Johnson', 50);

-- --------------------------------------------------------

--
-- Table structure for table `mod_order`
--

CREATE TABLE IF NOT EXISTS `mod_order` (
  `oid` varchar(10) NOT NULL,
  `pname` varchar(10) NOT NULL,
  `cname` varchar(10) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `price` varchar(10) NOT NULL,
  `mrp` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mod_order`
--

INSERT INTO `mod_order` (`oid`, `pname`, `cname`, `quantity`, `price`, `mrp`) VALUES
('36', '1', 'Ranbaxy', '10', '10', '100'),
('36', '1', 'Ranbaxy', '10', '10', '100'),
('36', '1', 'Ranbaxy', '10', '10', '100'),
('37', '2', 'Cipla', '10', '20', '200'),
('38', '1', 'Ranbaxy', '10', '10', '100'),
('42', '1', 'Ranbaxy', '100', '10', '1000'),
('', '1', 'Ranbaxy', '100', '10', '1000'),
('46', '1', 'Ranbaxy', '100', '10', '1000'),
('42', '1', 'Ranbaxy', '100', '10', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `price` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `user_id` int(5) NOT NULL AUTO_INCREMENT,
  `First_Name` varchar(15) NOT NULL,
  `Last_Name` varchar(15) NOT NULL,
  `Medical_Name` varchar(30) NOT NULL,
  `Medical_Location` varchar(15) NOT NULL,
  `Madical_Address` varchar(40) NOT NULL,
  `Pincode` int(7) NOT NULL,
  `Mobile_No` varchar(10) NOT NULL,
  `E_Mail` varchar(30) NOT NULL,
  `Password` varchar(25) NOT NULL,
  `varify` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `First_Name`, `Last_Name`, `Medical_Name`, `Medical_Location`, `Madical_Address`, `Pincode`, `Mobile_No`, `E_Mail`, `Password`, `varify`) VALUES
(42, 'kuldeep', 'Mourya', 'Royale', 'Juhu', 'dd eefhe efuef eefn2ef', 4000056, '9999999999', 'k.m.kuldeepmourya@gmail.com', '5284047f4f', 1),
(43, 'Akash', 'shah', 'Royali', 'Juhu', 'adgdg uhqw gjbwq', 4000056, '9999999999', 'Akash@gmail.com', '5284047f4f', 1),
(44, 'kuldeep', 'Mourya', 'Royali', 'Juhu', 'efwef efwefwf', 4000056, '9999999999', 'h@g', '5284047f4f', 1),
(45, 'ram', 'mhatre', 'fffysf hf', 'Juhu', 'hsjgh grs gusrg ugrrg', 4013234, '9987654897', 'abc@gmail.com', 'f5ca63e4c2', 1),
(46, 'kuldeep', 'maurya', 'Roysle', 'Juhu', 'ojzdf ssofhufdh', 4000056, '9999999999', 'jay@gmail.com', '5284047f4f', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
