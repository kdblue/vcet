-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 07:51 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `password`) VALUES
('nikhil', 'nikhil25'),
('kuldeep3', 'kdblue3'),
('mihir', 'mihir25');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_info`
--

CREATE TABLE IF NOT EXISTS `medicine_info` (
`medicine_id` int(5) NOT NULL,
  `company_name` varchar(30) NOT NULL,
  `medicine_price` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicine_info`
--

INSERT INTO `medicine_info` (`medicine_id`, `company_name`, `medicine_price`) VALUES
(1, 'Ranbaxy', 10),
(2, 'Cipla', 20),
(3, 'Himalaya', 30),
(4, 'Cipla', 40),
(5, 'Johnson', 50);

-- --------------------------------------------------------

--
-- Table structure for table `mod_order`
--

CREATE TABLE IF NOT EXISTS `mod_order` (
  `oid` varchar(10) NOT NULL,
  `pname` varchar(10) NOT NULL,
  `cname` varchar(10) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `price` varchar(10) NOT NULL,
  `mrp` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mod_order`
--

INSERT INTO `mod_order` (`oid`, `pname`, `cname`, `quantity`, `price`, `mrp`) VALUES
('36', '1', 'Ranbaxy', '10', '10', '100'),
('36', '1', 'Ranbaxy', '10', '10', '100'),
('36', '1', 'Ranbaxy', '10', '10', '100'),
('37', '2', 'Cipla', '10', '20', '200'),
('38', '1', 'Ranbaxy', '10', '10', '100'),
('33', '1', 'Ranbaxy', '20', '10', '200');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
`user_id` int(5) NOT NULL,
  `First_Name` varchar(15) NOT NULL,
  `Last_Name` varchar(15) NOT NULL,
  `Medical_Name` varchar(30) NOT NULL,
  `Medical_Location` varchar(15) NOT NULL,
  `Madical_Address` varchar(40) NOT NULL,
  `Pincode` int(7) NOT NULL,
  `Mobile_No` varchar(10) NOT NULL,
  `E_Mail` varchar(30) NOT NULL,
  `Password` varchar(15) NOT NULL,
  `varify` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `First_Name`, `Last_Name`, `Medical_Name`, `Medical_Location`, `Madical_Address`, `Pincode`, `Mobile_No`, `E_Mail`, `Password`, `varify`) VALUES
(29, 'nikhil', 'matre', 'Egle', 'Juhu', 'wwdjjwqhdjd', 4000056, '9999999999', 'j@q', '000000000000000', 1),
(30, 'gunda', 'coder', 'Egle', 'Juhu', 'kfjiwejfjiwfwfjw', 4000056, '9999999999', 'h@g', '000000000000000', 1),
(31, 'jaydeep', 'maurya', 'Amar', 'Irla', 'near juhu milinium club near gulmohor ro', 4000056, '9999999999', 'jay@g', '000000000000000', 1),
(32, 'mihir', 'thakur', 'Egle', 'Juhu', '2eefioh2oyfouf', 4000056, '9999999999', 'm@g', '000000000000000', 1),
(33, 'nihki', 'mhatre', 'Jai Sree', 'Andheri', 'ghbb hj hhrsddfc  yn uj h b56yb6', 4013030, '9011731303', 'nikhil@gmail.com', 'Nikhil24', 1),
(34, 'Prats', 'Kadz', 'Rameshwar', 'Andheri', 'bmghjtyr jhfyrt jhjrtryt', 4000056, '8108755555', 'abc@gmail.com', 'abc12345', 1),
(35, 'samerdddep', 'maurya', 'Royale', 'Juhu', 'jcgygwecwjcgheuc ecjbwejc eecjkbejc ', 4000056, '9999999999', 'samer@g', '000000000000000', 1),
(36, 'akshay', 'kumar', 'akky medi', 'Andheri', 'okay', 4012012, '9898989898', 'akii@g', '111111111111111', 1),
(37, 'ninad', 'mohite', 'ninad', 'Juhu', 'abffhttg', 4000920, '9773802695', 'ninadmn@gmail.com', 'ninad1995', 1),
(38, 'dfxa', 'hsgjhd', 'gfgdg', 'Juhu', 'uygtuytyu', 4000056, '9672219999', 'dghg@gamil.com', '12333333', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `medicine_info`
--
ALTER TABLE `medicine_info`
 ADD PRIMARY KEY (`medicine_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `medicine_info`
--
ALTER TABLE `medicine_info`
MODIFY `medicine_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
